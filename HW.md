# Lab 9

by Chernyshov Valentin (v.chernyshov@innopolis.university)

Site: [lichess.org](https://lichess.org)
guideline: [link](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=forgot%20password%20service%3A-,Return%20a%20consistent%20message%20for%20both%20existent%20and%20non%2Dexistent%20accounts.,-Ensure%20that%20the)

## Password reset

| Test step                                          | Result                                                                   |
|----------------------------------------------------|--------------------------------------------------------------------------|
| open [lichess.org](https://lichess.org)            | OK                                                                       |
| press sign in button                               | OK                                                                       |
| Try to sign in with legit credentials              | Home page with username was shown                                        |
| Sign out                                           | OK                                                                       |
| try to sign in with legit email and wrong password | Invalid username or password message shown                               |
| try to sign in with wrong email and wrong password | Invalid username or password message shown                               |
| Press "Password reset" button                      | Password reset dialog is opened                                          |
| Try to enter some string                           | String of format "`someString`@`someString`" is required                 |
| Enter legit email                                  | "Check your Email" message is shown with censored email almost instuntly |
| Return to Password reset page and enter string with format "`someString`@`someString`" | "Check your Email" message is shown with censored email almost instuntly |
| Try to sign in with legit credentials              | Home page with username was shown as on step 3                           |

## Results

1. The message for both valid and invalid emails is the same
2. The time to show message is practicaly the same (no additianal checks happens befor showing the massage)
3. Test case is successful

Result with valid account

![](./valid_email.png)

Result with account that does not exsist

![](./invalid_email.png)

Result without email

![](./invalid_string.png)
